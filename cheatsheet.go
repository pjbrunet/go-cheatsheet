package main

import (
	"fmt"
	"runtime"
	"math"
	"time"
	"strconv"
)

func add(x byte, y byte) (int, int) {
	var z = int(x + y) // casting is necessary because input values are bytes, not ints :-/
	return z, 7
}

func main() {
	
	// Play with returning multiple values of different types.
	var z1, z2 = add(2, 200)
	
	// z1 += z2 // if z2 isn't used, Go complains "z2 declared but not used"
	
	fmt.Println("My favorite # is", z1, z2)
	
	var os = runtime.GOOS
	
	// Switch versus if/else, IMO case/switch is a waste of time in every language.
	switch os {
		case "linux":
			fmt.Println("Linux")
		default:
			fmt.Println("Unknown OS")
	}
	
	// Easier to read, why bother with "switch" ?
	if os == "linux" {
		fmt.Println("Linux")
	} else {
		fmt.Println("Unknown OS")
	}
	
	type Name struct {
		first string
		last string
	}
	
	var pete = Name{"PJ", "Brunet"}
	fmt.Println(pete.first, pete.last)
	
	var joe = new(Name)
	joe.first = "Joe"
	fmt.Println("New Joe:", joe.first)
	
	var myArray = [3]int{1, 2, 3}
	myArray[0] = 0;
	fmt.Println(myArray)
	
	var namesArr [2]Name // array of structs (two Names)
	namesArr[0].first = "Joe"
	namesArr[0].last = "Blow"
	namesArr[1] = Name{"PJ", "Brunet"} // faster way to define a Name
	
	fmt.Println(namesArr)

	// I assume if Names has sub structs, this essentially adds more dimensions to the array.
	// https://www.golangprograms.com/go-language/struct.html
	// Note: Go doesn't complain if a struct is defined but not used.
	type User struct {
		name Name
		country string // What's the maximum string length? Apparently it's limited by RAM available https://stackoverflow.com/a/66810396/722796
		city string // We don't really need a 3d dimension for this, it can go in the 2nd dimension.
	}


	/* Unsigned Integers  
	byte in Go is an alias for uint8 meaning it is an integer value
	uint8 (8-bit unsigned integer whose range is 0 to 255 )
	uint16 (16-bit unsigned integer whose range is 0 to 65535 )
    uint32 (32-bit unsigned integer whose range is 0 to 4294967295 )
    uint64 (64-bit unsigned integer whose range is 0 to 18446744073709551615 ) 
	*/
	
	// We imported "math" to calculate MaxInt
	fmt.Println("\nMaxInt:", math.MaxInt)
	fmt.Println("MaxInt8:", math.MaxInt8)
	fmt.Println("MaxInt16:", math.MaxInt16)
	fmt.Println("MaxInt32:", math.MaxInt32)
	fmt.Println("MaxInt64 aka int:", math.MaxInt64)
	fmt.Println("MaxFloat32:", math.MaxFloat32)
	fmt.Println("MaxFloat64:", math.MaxFloat64)

	// Supposedly unixTime is "int64" but sometimes you need to cast "int" to make it work.
	var unixTime = time.Now().Unix()
	fmt.Println("\nThis doesn't work: "+ string(unixTime))
	// You can't print string(unixTime) because string() wrongly assumes your int64 is a Unicode code point LOL.
	// One workaround is to import "strconv"
	fmt.Println("Unix timestamp, base 10: "+ strconv.FormatInt(unixTime, 10))
	
	// This is wrong https://golangdocs.com/golang-int64-to-string-conversion
	// Note to self: golangdocs.com is bogus, the real docs are here https://pkg.go.dev/strconv
	// ERROR: fmt.Println("Unix timestamp, Itoa: "+ strconv.Itoa(unixTime))
	fmt.Println("Unix timestamp, Itoa: "+ strconv.Itoa(int(unixTime))) // Works but ugly.
	// ERROR: fmt.Println("Unix timestamp, Itoa: "+ strconv.Itoa(int64(unixTime)))
	
	// Can we declare "int" in advance? The answer is no, doesn't matter, don't waste your time. 
	// ERROR: var unixTime2 int = time.Now().Unix()
	// var unixTime2 int64 = time.Now().Unix() // Works, but doesn't make any difference.
	// ERROR: fmt.Println("Unix timestamp, Itoa: "+ strconv.Itoa(unixTime2))
	// fmt.Println("Unix timestamp, Itoa: "+ strconv.Itoa(int(unixTime2))) // Same as before.

	// Instead of converting unixTime to a string, it's easier to pass it as a parameter like this...
	fmt.Println("Unix timestamp, Println:", unixTime)
	fmt.Print("Unix timestamp, Print: ", unixTime, "\n") // https://stackoverflow.com/a/25932558/722796
	// Web development often involves concatenating long strings, where Sprint will be useful. 
	fmt.Println("Unix timestamp, Sprint: "+ fmt.Sprint(unixTime)) 
	
	// Display milliseconds elapsed
	startTime := time.Now().UnixMilli()
	time.Sleep(time.Millisecond * 15) // Sleep for 15 milliseconds.
	endTime := time.Now().UnixMilli()
	elapsedTime := endTime - startTime
	// https://stackoverflow.com/questions/32815400/how-to-perform-division-in-go
	// When the division operation has an untyped constant operand and a typed operand, the typed operand determines the type... 
	var elapsedSeconds float32 = float32(elapsedTime) / 1000
	fmt.Printf("\nSeconds elapsed: %.3fs", elapsedSeconds)
	fmt.Print("\nMilliseconds elapsed: ", elapsedTime, "ms")
	// Default verb is easy... 
	fmt.Printf("\n%v - %v = %v\n", startTime, endTime, elapsedTime)
		
	// GOLANG CONDITIONALS
	// ERROR non-boolean condition in if statement: if 1 { fmt.Println("1 evals to true? NOPE") }
	// ERROR non-boolean condition in if statement: if 0 { // Do nothing
	// } else { fmt.Println("0 evals to false? NOPE") }
	// If you REALLY want to use 1 or 0...
	if 1 == 1 { fmt.Println("\n1 == 1 is true.") }
	if 0 != 1 { fmt.Println("0 != 1 is true.") }
	// No parenthesis necessary, but braces are always necessary, even for single statements. 
	if 1 == 1 && 1 == 1 && (1 == 1) { fmt.Println("Test &&.") }
	// But you do need the braces.
	// ERROR: if (1 == 1) fmt.Println("Test braces.")
	
} // main()
